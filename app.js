const express = require('express');
const connectDB = require('./config/db');
const employeesRouter = require('./router/employees');
// import express from 'express';
// import connectDB from './config/db.js';
// import employeesRouter from './router/employees.js';
// import bodyParser from 'body-parser';
// import cors from 'cors';
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();

// db connection
connectDB();
// Use the PORT from environmental variable
const PORT = process.env.PORT;
const api = process.env.API_URL;

// Ths system noticed that the request is JSON
app.use(bodyParser.json());

// CORS Middleware
app.use(cors());

// routers for now only one router i.e employees
app.use(`/api/employees`, employeesRouter);

// server listen at port 4000 or PORT
app.listen(5001, () => {
	console.log(`Server running on @ 5000`);
});

module.exports = app;
