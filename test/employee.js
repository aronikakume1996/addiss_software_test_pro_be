const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app');

chai.should();
chai.use(chaiHttp);

describe('Employee', () => {
	//TEST GET ALL EMPLOYEE
	context('GET /api/employees', () => {
		it('It should GET all employees', async () => {
			const response = await chai.request(server).get('/api/employees');
			response.should.have.status(200);
			response.body.should.be.a('array');
		});
		it('It should NOT GET all employees with wrong URL', async () => {
			const response = await chai.request(server).get('/api/employee');
			response.should.have.status(404);
		});
	});

	// TEST POST EMPLOYEE
	context('GET /api/employees', () => {
		const employee = {
			name: 'Mebratu',
			DOB: '08/07/2022',
			gender: 'Male',
			salary: 7000,
		};
		it('It should POST employee to Database', async () => {
			const response = await chai
				.request(server)
				.get('/api/employees')
				.send(employee);
			response.should.have.status(200);
			response.body.should.be.a('array');
		});
	});
	// TEST PUT EMPLOYEE
	context('PUT /api/employees/:id', () => {
		const empID = '61ed276c4047ca16fcf9d54c';
		const employee = {
			name: 'Mebratu',
			DOB: '08/07/2022',
			gender: 'Male',
			salary: 7000,
		};
		it('It should PUT or UPDATE existing employee ', async () => {
			const response = await chai
				.request(server)
				.put('/api/employees/' + empID)
				.send(employee);
			response.should.have.status(200);
			response.body.should.be.a('object');
			response.body.should.have.property('name').eq('Mebratu');
			response.body.should.have.property('DOB').eq('08/07/2022');
			response.body.should.have.property('gender').eq('Male');
			response.body.should.have.property('salary').eq(7000);
		});
		it('It should PUT or UPDATE existing employee with wrong ID ', async () => {
			const empID = '61ed276c4047ca16fcf9d54d';
			const response = await chai
				.request(server)
				.put('/api/employees/' + empID)
				.send(employee);
			response.should.have.status(500);
		});
	});
	// TEST DELETE EMPLOYEE
	context('DELETE /api/employees/:id', () => {
		it('It should DELETE the employee in the Database', async () => {
			const empID = '61f7af7e762e5d14ba74c7b2';
			const response = await chai
				.request(server)
				.delete('/api/employees/' + empID);
			response.should.have.status(404);
			// response.body.should.be.a('object');
		});

		it('It should DELETE the employee in the Database with wrong ID', async () => {
			const empID = '61f7af7e762e5d14ba74c7b2';
			const response = await chai
				.request(server)
				.delete('/api/employees/' + empID);
			response.should.have.status(404);
		});
	});
});
