FROM node:16-alpine
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm start
COPY . .
EXPOSE 5000
CMD npm start