const Employee = require('../model/employee');
const express = require('express');
const router = express.Router();

// create employees
router.post(`/`, async (req, res) => {
	// create the employee object for the request from body/frontend
	let employee = new Employee({
		name: req.body.name,
		DOB: req.body.DOB,
		gender: req.body.gender,
		salary: req.body.salary,
	});

	employee = await employee.save();
	if (!employee) {
		return res.status(400).json({
			success: false,
			message: 'Employee Not Created!',
		});
	}
	// send to the body / frontend
	res.status(200).send(employee);
});
// List or Get employees
router.get(`/`, async (req, res) => {
	try {
		let employeeList = await Employee.find();
		if (!employeeList) {
			res.status(400).json({ success: false });
		}
		res.status(200).send(employeeList);
	} catch (err) {
		res.status(500).send('Something Wrong');
	}
});
// Update employee
router.put(`/:id`, async (req, res) => {
	let employeeToUpdate = await Employee.findByIdAndUpdate(
		req.params.id,
		{
			name: req.body.name,
			DOB: req.body.DOB ? req.body.DOB : req.body.default,
			gender: req.body.gender,
			salary: req.body.salary,
		},
		{ new: true }
	);
	if (!employeeToUpdate) {
		res.status(500).json({
			success: false,
			message: 'Employee Not Found!',
		});
	}
	res.status(200).send(employeeToUpdate);
});
// Remove or Delete employee
router.delete(`/:id`, async (req, res) => {
	try {
		let employeeToDelete = await Employee.findByIdAndRemove(req.params.id);
		if (!employeeToDelete) {
			return res.status(404).json({
				success: false,
				message: 'The Employee Not Found',
			});
		} else {
			return res.status(200).json({
				success: true,
				message: 'The Employee Deleted Successfully!',
			});
		}
	} catch (error) {
		res.status(500).json({
			success: false,
			message: error.Error,
		});
	}
});

// Export the router as the module
module.exports = router;
